# webChat
> 웹소켓 이용한 간단한 챗봇

## 목적
> 회사에서 기존에 PHP CI에 작업된 부트스트랩 프론트엔드를 리액트로 마이그레이션 하기로 했다.  
> 해당 부분에 사용할 기술스택들을 결정했고, 회사 사정으로 업무가 지연되어서  
> 프로젝트 진행전 시험 삼아서 간단히 ToDo를 만들게 있나해서 만들어본다.

## DIR STRUCTURE
- build 
> webpack build utils
- build/addons
> webpack addons
- public
> static root
- src
> dev root

## LICENSE
- MIT

## AUTHOR
- ISA (yoonjonglyu)