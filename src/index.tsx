import React from 'react';
import ReactDOM from 'react-dom';

import App from './app';

const render : Function = function (App : React.FC) {
    ReactDOM.render(
        <App />,
        document.getElementById("app")
    );
};

render(App);