import React from 'react';
import { Story, Meta } from '@storybook/react';

import App from './app';

export default {
  title: 'WebChat/App',
  component: App,
} as Meta;

const Template: Story<any> = (args) => <App {...args} />;

export const def = Template.bind({});
def.args = {
};
