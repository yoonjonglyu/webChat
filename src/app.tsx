import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

const App : React.FC = function () {
    return (
        <Router>
            <Switch>
                <Route exact path="/" />
            </Switch>
        </Router>
    );
}

export default App;